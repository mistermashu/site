<?php
class blogcategory extends site_model
{
	protected $table = 'blogcategories';
	
	public static function search($data=array())
	{
		// generate SQL to get blogpost_ids:
		$sql = 'SELECT id FROM blogcategories WHERE 1 ';
		
		// filters:
		if(!empty($data['name'])) $sql .= ' AND name LIKE "%'. str_replace(' ', '%', mysql_real_escape_string($data['name'])). '"';
		
		// order:
		if(empty($data['orderby'])) $data['orderby'] = 'name DESC';
		$sql .= ' ORDER BY '.$data['orderby'];
		
		// limit:
		if(!empty($data['limit'])) $sql .= ' LIMIT '.$data['limit'];
		
		// get blogcategory_ids:
		$blogcategories = array();
		foreach(site()->db->query($sql) as $blogcategory)
		{
			$blogcategories[] = new blogcategory($blogcategory['id']);
		}
		return $blogcategories;
	}
}