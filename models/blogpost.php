<?php
class blogpost extends site_model
{
	protected $table = 'blogposts';
	
	public static function search($data=array())
	{
		// generate SQL to get blogpost_ids:
		$sql = 'SELECT id FROM blogposts WHERE 1 ';
		if(!empty($data['category_id'])) $sql .= ' AND category_id = '.$data['category_id'];
		
		// handle selecting by month (i.e. "2014-08")
		if(!empty($data['month']))
		{
			$m = date('m', strtotime($data['month']));
			$y = date('Y', strtotime($data['month']));
			$startdate = date('Y-m-d', mktime(0, 0, 0, $m, 1, $y));
			$enddate = date('Y-m-d', mktime(0, 0, 0, $m, date('t', strtotime($data['month'])), $y));
			$sql .= ' AND date >= "'.$startdate.'" AND date <= "'.$enddate.'"';
		}
		
		// order:
		if(empty($data['orderby'])) $data['orderby'] = 'date DESC';
		$sql .= ' ORDER BY '.$data['orderby'];
		
		// limit:
		if(!empty($data['limit'])) $sql .= ' LIMIT '.$data['limit'];
		
		// get blogpost_ids:
		$blogposts = array();
		foreach(site()->db->query($sql) as $blogpost)
		{
			$blogposts[] = new blogpost($blogpost['id']);
		}
		return $blogposts;
	}
}