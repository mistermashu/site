<?php
// This is the main site() file. This file is the only thing you ever need to include in your php files.
// From then on, you can simply access a global site object via the site() function.
// For example, to set the template to 'page', simply say "site()->template->view = 'page';" from any context. Cool!
// There are a few advantages by doing it this way:
//	a. you can have raw php files or "static pages" that get all site() framework functionality, as well as controllers.
//	b. you can access any of the site() framework functionality the same from any context
//		.. if you've done a lot of codeigniter you know when you can say "$this->" and when you have to say "get_instance()->"
//		.. but just using "site()" all the time is much more consistent and copy/pasteable.

// require a few core classes:
require_once 'core/site_module.php';
require_once 'core/site_controller.php';
require_once 'core/site_model.php';

class site
{
	public static $site;
	private $modules = array();
	
	public function __construct()
	{
		session_start();
		
		// Register an autoload function used for models:
		/* CAN ONLY DO THIS ONCE WE'RE RUNNING PHP 5 or WHATEVER.. DELETE THE __autoload() FUNCTION BELOW IF THIS ENDS UP WORKING..
		spl_autoload_register(function($model){
			site()->load->model($model);
		});
		*/
		
		// determine site() path to load a few
		$sitepath = dirname(__FILE__);
		
		// Load the path object so our loader knows where to load stuff from
		require_once $sitepath.'/modules/path.php';
		$this->modules['path'] = new path();
		
		// Load the loader so we don't try to load the loader to load the loader later (only chuck norris can do that)
		require_once $sitepath.'/modules/load.php';
		$this->modules['load'] = new load();
	}
	
	// auto-load and access site modules:
	public function __get($key)
	{
		if(!isset($this->modules[$key]))
		{
			$this->modules[$key] = $this->load->module($key);
		}
		return $this->modules[$key];
	}
}

// Here, we'll define a function for attempting to autoload models:
// This way, you can just use the model classes without needing to include/require them.
function __autoload($model)
{
	if(!site()->load->model($model))
	{
		site()->debug->error('Class '.$model.' not found.');
	}
}

// instantiate site and define the global getter:
site::$site = new site();
function site()
{
	return site::$site;
}