General
===
site() is my php framework for building and maintaining websites and web apps. It is simple to use, efficient, and naturally produces easily reusable code if you want.


Getting Started
===
To start your code, simply include the 'site/site.php' file. From then on, everything the site() framework provides will be accessed via the global 'site()' function, hence my framework's name.


Modules
===
Modules are the lowest level concept in the site() framework. Modules simply contain global functions that you can access by calling them via the 'site()' function. For example, if I want to call a function called 'login()' from my 'access' module, I simply have to say 'site()->access->login()'.  The site framework dynamically loads the modules as you call them, so there is no downside of having lots of modules!

To create your own module called 'access' with a 'login()' function follow these steps:
1. Create a new file called 'access.php' in the 'site/modules/' directory.
2. Create a php class in your new file called access
3. Create your php 'login()' function.

Voila! It is that easy to create a global module. Now, on any of your sites, you can say 'site()->access->login()' to dynamically load your access class and call that function!

If you want to create a site-specific module, you can simply add a folder called 'modules' one directory above your document root. For example, for my site (mistermashu.com) has a document root of /home/mrmashu/public_html/ which holds all publicly facing files. To create a site-specific module, you can just put the file in /home/mrmashu/modules/ and prefix your class name with 'my_' so the file would be called 'access.php' and the class name would be 'my_access'

This way, the site module loading system can dynamically load your site-specific module and the global one at the same time!

If you want to exclude global module functions, simply extend the class 'site_module' instead of, for example, 'access'


Global AND site-specific code
===
One of the main reasons I really love the site() framework is its ability to seamlessly use both global code, site-specific code, and a combination of the two. Modules, Models, Controllers, and Views all have the ability to have both global and site-specific versions, so you can re-use code if it doesn't change for a different site, and only change what you need to if it does.

Below is a detailed technical description of some of the key differences between global and site-specific modules, models, controllers, and views

Modules (.e.g. 'access')
---
- global
	- filename: access.php
	- directory: site/modules/
	- class name: access
- site-specific
	- filename: access.php
	- directory: YOUR_SITE/modules/
	- class name: my_access

Models (e.g. 'user')
---
- global
	- filename: user.php
	- directory: site/models/
	- class name: user
- site-specific
	- filename: user.php
	- directory: YOUR_SITE/models/
	- class name: my_user

Controllers (e.g. 'users')
---
- global
	- filename: users.php
	- directory: site/controllers/
	- class name: users
- site-specific:
	- filename: users.php
	- directory: YOUR_SITE/controllers/
	- class name: my_users

Views (e.g. 'listusers')
---
- global
	- filename: listusers.php
	- directory: site/views/
	- class name: N/A (you do not make a class for views, you simply output HTML
- site-specific
	- filename: listusers.php
	- directory: YOUR_SITE/views/
	- class name: N/A (NOTE: site-specific views completely override global views.)



Controllers and the Router module
===
The site() framework is also well-equipped for large web apps via the sweet Controller system I created to replicate how CodeIgniter handles URLs, but with the option of having static html and php pages outside of the controller system, as well as being handled much more simply.

The first step of using controllers and the router module is to tell the router to route on your site's index page. Just after including the site/site.php file, just say 'site()->router->route()'
This will load the router module and call the route() function (which you can easily look at in the site/modules/ directory if you'd like!)

It essentially says the following:
1. If there is a file that matches that URL, load that file.
2. Otherwise, If there's a controller with a function that matches the URL, call that function.
3. Otherwise, err out with a 404.

This kind of controller setup is essential for creating web apps quickly. You can create php functions that magically turn in to web pages with just a few lines of code.

The only other thing you need is an .htaccess file that routes all requests to your index page, so that the router can handle everything. I have included the .htaccess file I use for that in the site/ directory so you can just copy it over. Note: it also handles stripping off .php extensions so your URLs will look really nice and SEO-friendly, but you can remove that if you like ugly things.

NOTE: if you are using global controllers, it is REQUIRED that you create a site-specific version to let the site know you want the global functionality in this site. This is a security feature because without this, you could load any gobal controller from any site, even if it doesn't belong on that site. For example, if you went to http://websitewithnopictures.com/pictures/ it would pick up a global pictures controller if there is one. So it does not do that. You must have a controller, even if it's empty, on a per-site basis.