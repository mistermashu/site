TODO
===

- change the load module's basephp function to assume it's the directory that the site/ directory is in.
- change modules, models, controllers, and views naming convention to the following:
	- all global stuff starts with global_
	- all local stuff has no prefix
	- make sure to change it in the loader module
- make controllers support directories
	- then, make a really nice set of admin tools and controllers for common stuff, utilizing the controllers/admin/ directory for the admin-specific stuff.
	- make isinstalled/install/validate/uninstall/widget functions for each admin tool standardized, so the superadmin can generate a list of all admin tools and provide install / uninstall buttons
- make a setup module that helps setting up and validating the architecture for a site framework site.
	- all directories
	- config file with some database stuff
	- index page with router
	- .htaccess file
- make an image controller that accepts a name and resolution and outputs the given image (or 404) based on those details.
	- make it cache each image at each resolution, so each image should get its own folder, then the image filename is the resolution.
	- images won't have to be hosted in publicly facing directory.
- break the load module away from using the path module
	- THEN, make the load module actually not a module, but rather a core class
	- remove the nasty includes in the site() constructor.
	- don't need sitepath calculation then either.