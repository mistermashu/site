<div id="quotes"></div>

<script type="text/javascript">
function listquotes(){
	action('quotes.listquotes',{},function(response){
		$('#quotes').html(response);
	});
}
function addquote(){
	dialog.open('quotes.addquote');
}
function createquote(data){
	action('quotes.addquote',data,function(response){
		listquotes();
		dialog.close();
	});
}
function editquote(quote_id){
	dialog.open('quotes.editquote',{quote_id:quote_id});
}
function savequote(data){
	action('quotes.editquote',data,function(response){
		listquotes();
		dialog.close();
	});
}
function deletequote(quote_id){
	dialog.choose('Are you sure you want to delete this quote?',{
		'No, keep it':function(){
			dialog.close();
		},
		'Yes, Delete it':function(){
			action('quotes.deletequote',{quote_id:quote_id},function(response){
				listquotes();
				dialog.close();
			});
		}
	});
}
listquotes();
</script>