<table>
	<caption>Quotes <a class="add button" onclick="addquote();">Add Quote</a></caption>
	<thead>
		<tr>
			<th>Quote</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
		<? foreach($quotes as $quote){?>
			<tr>
				<td><?=$quote['text']?></td>
				<td>
					<a class="edit icon" title="Edit Quote" onclick="editquote(<?=$quote['id']?>);"></a>
					<a class="delete icon" title="Delete Quote" onclick="deletequote(<?=$quote['id']?>);"></a>
				</td>
			</tr>
		<? }?>
	</tbody>
</table>