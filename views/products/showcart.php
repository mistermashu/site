<table>
	<thead>
		<tr>
			<th>Product</th>
			<th>Quantity</th>
			<th>Price</th>
			<th>Subtotal</th>
			<th>Remove</th>
		</tr>
	</thead>
	<tbody>
		<? if(empty($products)){?>
			<tr>
				<td colspan="5">There are currently no items in your shopping cart.</td>
			</tr>
		<? } else foreach($products as $product){?>
			<tr>
				<td><?=$product['name']?></td>
				<td><?=$product['quantity']?></td>
				<td>$<?=number_format($product['price'],2)?></td>
				<td>$<?=number_format($product['subtotal'],2)?></td>
				<td>
					<a class="remove icon" onclick="removeproduct(<?=$product['id']?>);"></a>
				</td>
			</tr>
		<? }?>
	</tbody>
</table>