<span id="product_images">
	<? foreach($images as $image){?>
		<img src="<?=$site->path->url('images/products/800x800/'.$image)?>" alt="Product Image"/>
	<? }?>
</span>


<h2><?=$product['name']?></h2>
<p><?=$product['description']?></p>
<span id="productprice">$<?=number_format($product['price'],2)?></span>
<? if($product['quantity']<=0){?>
	<span id="outofstock">Out of Stock!</span>
<? }else{?>
	<a onclick="addtocart('<?=$product['id']?>');">Buy Now!</a>
<? }?>

<script type="text/javascript">
function addtocart(product_id){
	action('products.addtocart',{product_id:product_id},function(response){
		window.location = '<?=$site->path->url('shopping-cart.php')?>';
	});
}
</script>