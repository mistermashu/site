<table>
	<caption>Link Groups <a class="add button" onclick="links_addlinkgroup();">Add Link Group</a></caption>
	<thead>
		<tr>
			<th>Name</th>
			<th>Links</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
		<? foreach($linkgroups as $linkgroup){?>
			<tr>
				<td><?=$linkgroup['name']?></td>
				<td><?=$site->db->query('SELECT COUNT(8) AS count FROM links WHERE linkgroup_id = '.$linkgroup['id'])->val('count')?></td>
				<td>
					<a class="edit icon" title="Edit <?=$linkgroup['name']?>" onclick="links_editlinkgroup(<?=$linkgroup['id']?>);"></a>
					<a class="delete icon" title="Delete <?=$linkgroup['name']?>" onclick="links_deletelinkgroup(<?=$linkgroup['id']?>);"></a>
				</td>
			</tr>
		<? }?>
	</tbody>
</table>


<script type="text/javascript">
function links_addlinkgroup(){
	dialog.open('links.addlinkgroup');
}
function links_createlinkgroup(data){
	action('links.addlinkgroup',data,function(response){
		refreshwidget('links');
		dialog.close();
	});
}
function links_editlinkgroup(linkgroup_id){
	dialog.open('links.editlinkgroup',{linkgroup_id:linkgroup_id});
}
function links_savelinkgroup(data){
	action('links.editlinkgroup',data,function(response){
		refreshwidget('links');
		dialog.close();
	});
}
function links_deletelinkgroup(linkgroup_id){
	dialog.choose('Are you sure you want to delete this Link Group?',{
		'No, Keep it':function(){
			dialog.close();
		},
		'Yes, Delete it':function(){
			action('links.deletelinkgroup',{linkgroup_id:linkgroup_id},function(response){
				refreshwidget('links');
				dialog.close();
			});
		}
	});
}
</script>