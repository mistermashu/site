<? foreach($linkgroups as $linkgroup){?>
	<table>
		<caption><?=$linkgroup['name']?> <a class="add button" onclick="addlink(<?=$linkgroup['id']?>);">Add Link</a></caption>
		<thead>
			<tr>
				<th>Name</th>
				<th>Description</th>
				<th>URL</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			<? foreach($linkgroup['links'] as $link){?>
				<tr>
					<td><?=$link['name']?></td>
					<td><a class="info icon" title="<?=$link['description']?>"></a></td>
					<td><a target="_blank" href="<?=$link['url']?>"><?=$link['url']?></a></td>
					<td>
						<a class="delete icon" onclick="deletelink(<?=$link['id']?>);"></a>
					</td>
				</tr>
			<? }?>
		</tbody>
	</table>
<? }?>