<span id="addlinkform" class="form">
	<input type="hidden" name="linkgroup_id" value="<?=$linkgroup['id']?>"/>
	<p>
		<label for="name">Name</label>
		<input type="text" name="name" id="name" placeholder="Name.."/>
	</p>
	<p>
		<label for="description">Description</label>
		<textarea name="description" id="description" rows="5" cols="50"></textarea>
	</p>
	<p>
		<label for="url">URL <a class="info icon" title="Please include protocol if applicable (i.e. http://)"></a></label>
		<input type="text" name="url" id="url" placeholder="URL.."/>
	</p>
	<p>
		<label></label>
		<a class="add button" onclick="createlink(formdata('#addlinkform'));">Create Link</a>
	</p>
</span>