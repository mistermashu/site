<h2>Links</h2>

<div id="links"></div>

<script type="text/javascript">
function listlinks(){
	action('links.listlinks',{},function(response){
		$('#links').html(response);
	});
}
function addlink(linkgroup_id){
	dialog.open('links.addlink',{linkgroup_id:linkgroup_id});
}
function createlink(data){
	action('links.addlink',data,function(response){
		listlinks();
		dialog.close();
	});
}
function deletelink(link_id){
	dialog.choose('Are you sure you want to delete this link?',{
		'No, Keep it':function(){
			dialog.close();
		},
		'Yes, Delete it':function(){
			action('links.deletelink',{link_id:link_id},function(response){
				listlinks();
				dialog.close();
			});
		}
	});
}
listlinks();
</script>