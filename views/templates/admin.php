<html>
	<head>
		<title>Admin Tools</title>
		<link rel="stylesheet" href="<?=site()->path->resources('css/admin.css')?>"/>
		<script type="text/javascript" src="<?=site()->path->resources('js/jquery-2.0.2.min.js')?>"></script>
		<script type="text/javascript" src="<?=site()->path->resources('js/admin.js')?>"></script>
	</head>
	<body>
		<header>
			<h1>Site Admin</h1>
			<a href="<?=site()->path->url('admin/logout')?>">Log Out</a>
		</header>
		<nav><?
			if(!empty($_SESSION['user_id']))
			{
				foreach($navlinks as $uri=>$name)
				{
					?><a href="<?=site()->path->url($uri)?>"><?=$name?></a><?
				}
			}
		?></nav>
		<section id="content"><?=$content?></section>
		<footer>
			<p>Please email me at <a href="mailto:mistermashu@gmail.com">mistermashu@gmail.com</a> with any questions.</p>
		</footer>
	</body>
</html>