<script src="//tinymce.cachefly.net/4.0/tinymce.min.js"></script>
<script type="text/javascript">
tinymce.init({
    selector: "textarea",
    plugins: [
        "advlist autolink lists link image charmap print preview anchor",
        "searchreplace visualblocks code fullscreen",
        "insertdatetime media table contextmenu paste"
    ],
    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
});
</script>

<h2>Edit Blog Post</h2>
<form action="<?=site()->path->current_url()?>" method="post">
	<table>
		<tr>
			<th><label for="title">Title</label></th>
			<td><input type="text" name="title" id="title" placeholder="Title.." value="<?=$blogpost->title?>"/></td>
		</tr>
		<tr>
			<th><label for="category_id">Category</label></th>
			<td>
				<select name="category_id" id="category_id">
					<option value="0">&mdash; Choose One &mdash;</option>
					<? foreach(blogcategory::search() as $blogcategory){?>
						<option value="<?=$blogcategory->id?>"<?=($blogpost->category_id == $blogcategory->id)?' selected="selected"':''?>><?=$blogcategory->name?></option>
					<? }?>
				</select>
			</td>
		</tr>
		<tr>
			<th><label for="text">Text</label></th>
			<td>
				<textarea name="text" id="text" rows="10" cols="50"><?=$blogpost->text?></textarea>
			</td>
		</tr>
		<tr>
			<th><label for="date">Date</label></th>
			<td><input type="date" name="date" id="date" value="<?=(empty($blogpost->date))?date('Y-m-d'):$blogpost->date?>"/></td>
		</tr>
		<tr>
			<td></td>
			<td>
				<a class="button" href="<?=site()->path->url('admin/blog')?>">Cancel</a>
				<input class="button" type="submit" value="Save"/>
			</td>
		</tr>
	</table>
</form>