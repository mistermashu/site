<h2>Blog Posts<a class="button" href="<?=site()->path->url('admin/blog/edit/0')?>">Create New Blog Post</a></h2>

<table>
	<thead>
		<tr>
			<th>Title</th>
			<th>Date</th>
			<th>Actions</th>
		</tr>
	</thead>
	<tbody>
		<? foreach($blogposts as $blogpost){?>
			<tr>
				<td><?=$blogpost->title?></td>
				<td><?=date('m/d/Y',strtotime($blogpost->date))?></td>
				<td>
					<a class="inline red button" href="<?=site()->path->url('admin/blog/delete/'.$blogpost->id)?>" onclick="return confirm('Are you sure you want to delete this blog post?');">Delete</a>
					<a class="inline button" href="<?=site()->path->url('admin/blog/edit/'.$blogpost->id)?>">Edit</a>
				</td>
			</tr>
		<? }?>
	</tbody>
</table>