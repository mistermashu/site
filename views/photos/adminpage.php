<style type="text/css">
.album{
	border-radius:1em;
	margin:1em;
}
.album img{
	display:inline-block;
	max-width:200px;
	border:1px solid #ddd;
	padding:0;
	margin:.5em;
	cursor:pointer;
}
.album .add{
	background-color:#bbb;
}
.album img:hover{
	background-color:#ddd;
}
</style>

<? if(!empty($error)){?>
	<p class="error"><?=$error?></p>
<? }?>

<? foreach($albums as $album){?>
	<fieldset class="album">
		<legend><?=$album['name']?></legend>
		<? foreach($album['photos'] as $photo){?>
			<img src="<?=$this->src(200,150,$photo['id'])?>" alt="Photo" onclick="deletephoto(<?=$photo['id']?>);"/>
		<? }?>
		<img class="add" src="<?=$site->path->resources('images/admin/addphoto.png')?>" alt="Add Photo to <?=$album['name']?>" onclick="addphoto(<?=$album['id']?>);"/>
	</fieldset>
<? }?>

<script type="text/javascript">
function addphoto(album_id){
	dialog.open('photos.uploadphoto',{album_id:album_id});
}
function deletephoto(photo_id){
	dialog.choose('Delete this photo?',{
		'No, Keep it':function(){
			dialog.close();
		},
		'Yes, Delete it':function(){
			action('photos.deletephoto',{photo_id:photo_id},function(response){
				window.location.reload();
			});
		}
	});
}
</script>