<style type="text/css">
#album-nav .active{
	background-color:blue;
}
#popup{
	display:none;
	position:absolute;
	border:1px solid #888;
	padding:1em;
	border-radius:1em;
	box-shadow:3px 3px 3px #888;
	background-color:white;
}
#popup img{
	display:inline-block;
	max-width:400px;
	max-height:300px;
}
</style>

<div id="album-nav">
	<? foreach($albums as $album){?>
		<a href="<?=$site->path->url('photos.php')?>?album_id=<?=$album['id']?>"<?=($_GET['album_id']==$album['id'])?' class="active"':''?>><?=$album['name']?></a>
	<? }?>
</div>

<?

// Display photos from a selected album:
if(isset($_GET['album_id']))
{
	// Find the album we're working with:
	$currentalbum = array();
	foreach($albums as $album)
	{
		if($album['id']==$_GET['album_id'])
		{
			$currentalbum = $album;
		}
	}
	
	// Make sure we found an album:
	if(empty($currentalbum))
	{
		die('<p>There is no album '.$_GET['album_id'].'</p>');
	}
	
	// Display all photos in this album:
	?>
	<div id="photos">
		<? foreach($currentalbum['photos'] as $photo){?>
			<a href="<?=$this->src(1600,1200,$photo['id'])?>" rel="lightbox[photos]">
				<img src="<?=$this->src(200,150,$photo['id'])?>" alt="Photo"/>
			</a>
		<? }?>
	</div>
	<div id="popup">
		<img src="" alt="Photo"/>
	</div>
	<?
}

?>

<script type="text/javascript">

// Initialize stuff:
$(function(){
	// The hover effect:
	$('#photos img').hover(function(){
		// Pop up larger image:
		var src = $(this).attr('src');
		src = src.replace('200x150/','800x600/');
		$('#popup img').attr('src',src);
		
		// position and display the popup:
		$('#popup').show();
		var offset = $(this).offset();
		offset.top -= $('#popup').height() + 40;
		offset.left -= ($('#popup').width() / 2) - ($(this).width() / 2);
		$('#popup').offset(offset);
	},function(){
		// Hide the popup:
		$('#popup').hide();
	});
});

</script>