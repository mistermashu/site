<?php
class site_controller
{
	// Overridable functions:
	public function admin_page()
	{
		site()->debug->error('Controller "'.$this->controller_name().'" has not defined "admin_page()" yet.');
	}
	public function admin_install()
	{
		site()->debug->error('Controller "'.$this->controller_name().'" has not defined "admin_install()" yet.');
	}
	public function admin_uninstall()
	{
		site()->debug->error('Controller "'.$this->controller_name().'" has not defined "admin_uninstall()" yet.');
	}
	public function admin_widget()
	{
		site()->debug->error('Controller "'.$this->controller_name().'" has not defined "admin_widget()" yet.');
	}
}