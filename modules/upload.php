<?php
class upload extends site_module
{
	/*
	====Upload an image and save it according to the site->config's image profiles:
	example imageprofile (in config.php):
	'imageprofiles'=>array(
		'product_image'=>array(
			'templates'=>array(
				array(
					'path'=>'images/products/',
					'width'=>'300',
					'height'=>'400',
				),
				array(
					'path'=>'images/product_thumbs/',
					'width'=>'150',
					'height'=>'200',
				),
			),
		),
	),
	
	
	With the above example, a sample call would be like this:
	<input type="file" name="product_image"/>
	
	Then on form handling, the PHP call would be like this:
	
	$status = $site->upload->image('product_image');
	if($status->success)
	{
		$filename = $status->filename;
	}
	else
	{
		$error = $status->error;
	}
	
	
	*/
	public function image($profilename)
	{
		global $site;
		
		// get the image upload profile:
		if(isset($site->config->imageprofiles[$profilename]))
		{
			$profile = $site->config->imageprofiles[$profilename];
		}
		else
		{
			return new upload_status(false,'No Image Upload Profile called "'.$profile.'".. please specify in config->imageprofiles["'.$profile.'"]');
		}
		
		// get the file we're uploading:
		if(empty($_FILES[$profilename]))
		{
			return new upload_status(false,'No file in $_FILES called "'.$profilename.'".. check the "name" in the form, and make sure your form tag has the enctype="multipart/form-data" attribute.');
		}
		$file = $_FILES[$profilename];
		
		// Make sure there isn't an upload error:
		if($file['error'] > 0)
		{
			return new upload_status(false,'File upload error code: '.$file['error'].' go to <a href="http://php.net/manual/en/features.file-upload.errors.php">this page</a> to figure out what that means.');
		}
		
		// Make sure it's an image file:
		if(substr($file['type'],0,6)!='image/')
		{
			return new upload_status(false,'The file you tried to upload was not an image. Technically, it was of type "'.$file['type'].'" Please try again with an image file.');
		}
		
		// Find the final $filename, considering existing files
		$filename = $file['name'];
		$lookagain = true;
		while($lookagain)
		{
			$lookagain = false;
			foreach($profile['templates'] as $template)
			{
				$tmp_filename = pathinfo($this->getfilename('public_html/'.$template['path'].$filename),PATHINFO_BASENAME);
				if($tmp_filename != $filename) // if this file already exists
				{
					// NEXT.
					$filename = $tmp_filename;
					$lookagain = true;
					continue;
				}
			}
		}
		
		// Scale and save all images:
		$site->image->load($file['tmp_name']);
		foreach($profile['templates'] as $template)
		{
			$path = $site->path->php('public_html/'.$template['path']);
			
			// so an error isn't thrown the first time uploading an image (and so we don't have to worry about directories at all)
			if(!is_dir($path)) mkdir($path,0777,true);
			
			$site->image->scale($template['width'],$template['height']);
			$site->image->save($path.$filename);
		}
		
		// Success!
		return new upload_status(true,$filename);
	}
	
	// Upload a file in general:
	// $name is the name of the file from the form
	// $path is the path to store the file relative to the site's base directory (i.e. "whiskeysgrill")
	public function file($name,$path)
	{
		global $site;
		
		// Make sure the $name is right:
		if(empty($_FILES[$name]))
		{
			return new upload_status(false,'No file in $_FILES called "'.$profile.'".. check the "name" in the form, and make sure your form tag has the enctype="multipart/form-data" attribute.');
		}
		$file = $_FILES[$name];
		
		// Make sure there isn't an upload error:
		if($file['error'] > 0)
		{
			return new upload_status(false,'File upload error code: '.$file['error'].' go to <a href="http://php.net/manual/en/features.file-upload.errors.php">this page</a> to figure out what that means.');
		}
		
		// Get the filename:
		$filename = $this->getfilename($path.$file['name']);
		
		// Move the uploaded file to the right spot:
		move_uploaded_file($file['tmp_name'],$site->path->php($filename));
		
		// Return success!
		return new upload_status(true,$filename);
	}
	
	
	// this function returns the filename for a file that for sure doesn't exist already
	// i.e. if product.png already exists, if you pass 'images/product.png', it'll return 'images/product2.png'
	// NOTE: $filename should be relative to the site's root directory (the folder that public_html is in)
	// it also returns a path relative to the site's root directory
	private function getfilename($filename)
	{
		global $site;
		
		// Remove # from filename:
		$filename = str_replace('#','',$filename);
		
		$pathinfo = pathinfo($filename);
		$count = 2;
		while(file_exists($site->path->php($filename)))
		{
			$filename = $pathinfo['dirname'].'/'.$pathinfo['filename'].$count.'.'.$pathinfo['extension'];
			++$count;
		}
		
		return $filename;
	}
}

class upload_status
{
	public $success = FALSE;
	public $error = '';
	public $filename = '';
	
	// Pass the status (true/false) and either the error or the filename
	public function __construct($success,$string)
	{
		$this->success = $success;
		
		if($success === true)
		{
			$this->filename = $string;
		}
		else
		{
			$this->error = $string;
		}
	}
}