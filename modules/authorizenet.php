<?php
class authorizenet extends site_module
{
	private $transaction_id = 0;
	private $error_message = '';
	
	public function __construct()
	{
		global $site;
		
		// Validate config variables:
		$site->config->authorizenet_api_login_id;
		$site->config->authorizenet_transaction_key;
	}
	
	// Attempts to make a payment.
	// Returnes TRUE on success or FALSE on failure
	// a successful payment will set $this->transaction_id
	public function makepayment($amount,$cardnum,$expdate)
	{
		global $site;
		require_once $site->path->basephp('site/lib/anet_php_sdk/AuthorizeNet.php');
		$transaction = new AuthorizeNetAIM($site->config->authorizenet_api_login_id, $site->config->authorizenet_transaction_key);
		$transaction->amount = $amount;
		$transaction->card_num = $cardnum;
		$transaction->exp_date = $expdate;
		$response = $transaction->authorizeAndCapture();
		
		if($response->approved){
			$this->transaction_id = $response->transaction_id;
			return true;
		}else{
			$this->error_message = $response->error_message;
			return false;
		}
	}
	
	public function transaction_id()
	{
		return $this->transaction_id;
	}
	public function error_message()
	{
		return $this->error_message;
	}
}