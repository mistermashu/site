<?php

// This module provides useful information about the site
class info extends site_module
{
	public function controllers()
	{
		$files = scandir(site()->path->php('controllers'));
		
		$controllers = array();
		foreach($files as $key=>$file)
		{
			if($file != '.' && $file != '..')
			{
				$controllers[] = pathinfo($file, PATHINFO_FILENAME);
			}
		}
		
		return $controllers;
	}
}