<?php

/*
============USAGE EXAMPLE============
$site->image->load('images/products/comb.jpg');
$site->image->scale(200,300);
$site->image->save('images/products_thumb/comb.jpg');
*/


class image extends site_module
{
	private $image;
	private $image_type;
	private $compression = 95;
	
	public function load($filename)
	{
		if(!file_exists($filename))
		{
			site()->debug->error('Filename "'.$filename.'" does not exist in image->load($filename)');
			exit 1;
		}
		
		// Load image data:
		$image_info = getimagesize($filename);
		$this->image_type = $image_info[2];
		if($this->image_type == IMAGETYPE_JPEG )
		{
			$this->image = imagecreatefromjpeg($filename);
		}
		elseif($this->image_type == IMAGETYPE_GIF)
		{
			$this->image = imagecreatefromgif($filename);
		}
		elseif($this->image_type == IMAGETYPE_PNG)
		{
			$this->image = imagecreatefrompng($filename);
		}
		else
		{
			site()->debug->error('File type '.$this->image_type.' not supported in image->load($filename)');
			exit 1;
		}
	}
	
	public function save($filename)
	{
		if($this->image_type == IMAGETYPE_JPEG)
		{
			imagejpeg($this->image,$filename,$this->compression);
		}
		elseif($this->image_type == IMAGETYPE_GIF)
		{
			imagegif($this->image,$filename);
		}
		elseif($this->image_type == IMAGETYPE_PNG)
		{
			imagepng($this->image,$filename);
		}
		else
		{
			die('File type '.$this->image_type.' not supported in image->save($filename)');
		}
	}
	
	public function output()
	{
		header('Content-Type: '.image_type_to_mime_type($this->image_type));
		$this->save(NULL);
	}
	
	public function width()
	{
		return imagesx($this->image);
	}
	public function height()
	{
		return imagesy($this->image);
	}
	
	public function convert($type)
	{
		if($type == 'jpg')
		{
			$this->image_type = IMAGETYPE_JPEG;
		}
		elseif($type == 'gif')
		{
			$this->image_type = IMAGETYPE_GIF;
		}
		elseif($type == 'png')
		{
			$this->image_type = IMAGETYPE_PNG;
		}
		else
		{
			site()->debug->error('Error in image->convert($type).. only "jpg", "gif", and "png" are supported.');
			exit 1;
		}
	}
	
	// set the compression value of a JPEG image
	public function compression($value)
	{
		if($value < 0) die('You cannot have a compression value that is less than zero.');
		if($value > 100) die('You cannot have a compression value that is more than 100.');
		
		if($this->image_type == IMAGETYPE_JPEG)
		{
			$this->compression = $value;
		}
		else
		{
			die('Compression is only supported for JPEG images.');
		}
	}
	
	// scales the image to fit within the given $x,$y and maintains aspect ratio
	public function scale($x,$y)
	{
		$width = $this->width();
		$height = $this->height();
		
		$ratio = min($x / $width, $y / $height);
		$x = $ratio * $width;
		$y = $ratio * $height;
		
		// resize it!
		$this->resize($x,$y);
	}
	
	
	// strictly resizes the image to have the $x and $y coordinates
	// NOTE: this means it does not maintain the aspect ratio. for that, call scale($x,$y)
	public function resize($x,$y)
	{
		$newimage = imagecreatetruecolor($x,$y);
		imagecopyresampled($newimage,$this->image,0,0,0,0,$x,$y,$this->width(),$this->height());
		imagedestroy($this->image);
		$this->image = $newimage;
	}
	
	// crops the image from $x1,$y1 to $x2,$y2
	public function crop($x1,$y1,$x2,$y2)
	{
		// normalize coordinates so it's top-left to bottom-right:
		if($x1 > $x2)
		{
			$temp = $x1;
			$x1 = $x2;
			$x2 = $temp;
		}
		
		if($y1 > $y2)
		{
			$temp = $y1;
			$y1 = $y2;
			$y2 = $temp;
		}
		
		// crop the image:
		$width = $x2 - $x1;
		$height = $y2 - $y1;
		$newimage = imagecreatetruecolor($width,$height);
		imagecopyresampled($newimage,$this->image,0,0,$x1,$y1,$width,$height,$width,$height);
		imagedestroy($this->image);
		$this->image = $newimage;
	}
	
	// rotates the image a given amount of degrees
	public function rotate($angle,$bgd_color=0)
	{
		$this->image = imagerotate($this->image,$angle,$bgd_color);
	}
}