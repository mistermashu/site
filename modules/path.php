<?php

// This module is for getting information regarding server paths as well as URLs.
class path extends site_module
{
	private $url;
	private $documentroot; // the absolute path to the folder in which hostable files are kept (index.php, css files, js files, images, etc.)
	private $basepath; // the absolute path in which non-public php files are kept (controllers folder, views folder, models folder, config.php, public_html folder, etc.)
	
	public function __construct()
	{
		$this->url = 'http'.((empty($_SERVER['HTTPS']))?'':'s').'://'.$_SERVER['HTTP_HOST'].'/';
		
		// determine the document root:
		if(isset($_SERVER['DOCUMENT_ROOT']))
		{
			$this->documentroot = $_SERVER['DOCUMENT_ROOT'].'/';
		}
		elseif(!empty($_SERVER['argv']))
		{
			$this->documentroot = reset($_SERVER['argv']).'/'; // this works for command line calls (like for cron jobs)
		}
		else
		{
			site()->debug->error('unable to determine document root in path module.');
			exit 1;
		}
		
		// generate the base path by just stripping off the last folder:
		$this->basepath = dirname($this->documentroot).'/';
	}
	
	// returns the full canonical url to the current page
	public function current_url()
	{
		return $this->url.ltrim($_SERVER['REQUEST_URI'],'/');
	}
	
	// returns the full canonical url to the given path (relative to i.e. 'http://www.mistermashu.com/')
	public function url($path='')
	{
		return $this->url.$path;
	}
	
	// redirects to the given $path
	public function redirect($path='')
	{
		header('Location: '.$this->url.$path);
		die();
	}
	
	// returns the path to the base directory for this server account that has php files (i.e. '/home/mrmashu/'.$path)
	public function basephp($path='')
	{
		return '/home/mrmashu/'.$path;
	}
	
	// returns the full path to the $path specified relative to the site's directory (i.e. '/home/mrmashu/knightsofgemena/'.$path)
	public function php($path='')
	{
		return $this->basepath.$path;
	}
	
	// returns the canonical URL to the Green Gear Designs resources folder, appending the given $path
	public function resources($path='')
	{
		return 'http://resources.mistermashu.com/'.$path;
	}
	
	// use this function to test whether we are currently on this page:
	public function iscurrent($path='')
	{
		if($path == ltrim($_SERVER['REQUEST_URI'],'/')) return true;
		return false;
	}
}