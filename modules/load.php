<?php
// you can't override this module because of how it's loaded in the site constructor.. sorry.
// Let MisterMashu (mistermashu@gmail.com) know if you want him to figure out a way to do that.
// basically, this class is good for loading in models, views, controllers, and modules in such a way where you can have global files and site-specific files and it handles it well.
class load extends site_module
{
	public function view($name,$data=array())
	{
		// Set up variables for the view:
		extract($data);
		
		// Check for a site-specific view:
		$siteviewpath = site()->path->php('views/'.$name.'.php');
		if(file_exists($siteviewpath))
		{
			include $siteviewpath;
			return true;
		}
		
		// Check for a global view (in /site/views/)
		$globalviewpath = site()->path->basephp('site/views/'.$name.'.php');
		if(file_exists($globalviewpath))
		{
			include $globalviewpath;
			return true;
		}
		
		// Otherwise, err out:
		site()->debug->error('View '.$name.' not found in site-specific ('.$siteviewpath.') or site-global views ('.$globalviewpath.') directory.');
	}
	
	// Returns an object of type $name controller
	public function controller($name)
	{
		$controllerpath = site()->path->basephp('site/controllers/'.$name.'.php');
		$custompath = site()->path->php('controllers/'.$name.'.php');
		
		$class = '';
		
		// first see if there's a site-wide module:
		if(is_file($controllerpath))
		{
			require_once $controllerpath;
			$class = $name;
			
			// if it didn't define this class, err out to let the dev know the file is wrong:
			if(!in_array($class, get_declared_classes()))
			{
				site()->debug->error('Your controller class in '.$controllerpath.' must be named "'.$class.'"');
			}
		}
		
		// now see if there's a custom one for this particular web site:
		if(is_file($custompath))
		{
			require_once $custompath;
			$class = 'my_'.$name;
			
			if(!in_array($class, get_declared_classes()))
			{
				site()->debug->error('You must create a controller class called "'.$class.'" in '.$custompath.'. It can extend "'.$name.'" to extend additional functionality, or "site_controller" to start from scratch.');
			}
		}
		
		if(empty($class))
		{
			site()->debug->error('Controller '.$name.' not found in global or site-specific context.');
		}
		else
		{
			return new $class();
		}
	}
	
	// requires the model file(s) and returns TRUE if successful or FALSE if unsuccessful
	public function model($name)
	{
		$success = false;
		
		// Check for global model file:
		$modelpath = site()->path->basephp('site/models/'.$name.'.php');
		if(file_exists($modelpath))
		{
			$success = true;
			require_once $modelpath;
		}
		
		// Check for site-specific model file:
		$modelpath = site()->path->php('models/'.$name.'.php');
		if(file_exists($modelpath))
		{
			$success = true;
			require_once $modelpath;
		}
		
		return $success;
	}
	
	// requires the module file and a potential extended one, instantiates one and returns it:
	public function module($name)
	{
		$modulepath = site()->path->basephp('site/modules/'.$name.'.php');
		$custompath = site()->path->php('modules/'.$name.'.php');
		
		$class = '';
		
		// first see if there's a site-wide module:
		if(is_file($modulepath))
		{
			require_once $modulepath;
			$class = $name;
		}
		
		// now see if there's a custom one for this particular web site:
		if(is_file($custompath))
		{
			require_once $custompath;
			$class = 'my_'.$name;
		}
		
		if(empty($class))
		{
			site()->debug->error('Module '.$name.' not found in global or site-specific context.');
		}
		else
		{
			return new $class();
		}
	}
}