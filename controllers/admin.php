<?php
class admin extends site_controller
{
	public function __construct()
	{
		// enforce login other than on the "login" page:
		if($_SERVER['REQUEST_URI'] != '/admin/login')
		{
			if(!site()->access->isloggedin())
			{
				site()->path->redirect('admin/login');
			}
		}
		
		// set the template to the admin template:
		site()->template->view = 'admin';
		
		// get a list of all controllers for this site, then see which ones have admin() functions.
		site()->template->navlinks = self::getnavlinks();
	}
	
	private static function getnavlinks()
	{
		$navlinks = array();
		foreach(site()->info->controllers() as $controllername)
		{
			if($controllername != 'admin')
			{
				$controller = site()->load->controller($controllername);
				if(method_exists($controller, 'admin'))
				{
					$navlinks['admin/'.$controllername] = $controllername;
				}
			}
		}
		return $navlinks;
	}
	
	public function index()
	{
		site()->load->view('admin/index');
	}
	
	public function login()
	{
		$error = '';
		
		// handle logging in:
		if(isset($_POST['username']) && isset($_POST['password']))
		{
			$success = site()->access->login($_POST['username'], $_POST['password']);
			if($success)
			{
				site()->path->redirect('admin');
			}
			else
			{
				$error = 'Unable to authenticate. Please try again.';
			}
		}
		
		// show the log in form:
		site()->load->view('admin/login',array(
			'error'=>$error,
		));
	}
	
	public function logout()
	{
		site()->access->logout();
		site()->path->redirect('admin');
	}
	
	public function blog($action='index',$id=0)
	{
		if($action=='index')
		{
			// show blog posts:
			$blogposts = blogpost::search();
			site()->load->view('admin/blog/index',array(
				'blogposts'=>$blogposts,
			));
		}
		elseif($action=='edit')
		{
			// handle submissions:
			$blogpost = new blogpost($id);
			if(!empty($_POST))
			{
				if(empty($id)) $blogpost = new blogpost($_POST);
				else $blogpost->update($_POST);
				site()->path->redirect('admin/blog');
			}
			site()->load->view('admin/blog/edit',array(
				'blogpost'=>$blogpost,
			));
		}
		elseif($action=='delete')
		{
			$blogpost = new blogpost($id);
			$blogpost->delete();
			site()->path->redirect('admin/blog');
		}
	}
}